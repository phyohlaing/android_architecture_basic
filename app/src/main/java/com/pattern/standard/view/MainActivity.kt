package com.pattern.standard.view


import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.pattern.standard.R
import com.pattern.standard.model.Photo
import com.pattern.standard.model.PhotoService
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.net.URL
import android.os.AsyncTask
import android.widget.ImageView
import java.lang.ref.WeakReference


class MainActivity : AppCompatActivity() {

    private val retrofit = Retrofit.Builder()
            .baseUrl("https://jsonplaceholder.typicode.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()!!


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val service = retrofit.create(PhotoService::class.java)
        val photoCall = Photo.getPhotoCall(service)

        photoCall.enqueue(object : Callback<Photo> {
            override fun onResponse(call: Call<Photo>?, response: Response<Photo>?) {
                if (response != null && response.isSuccessful) {
                    val photo = response.body()

                    //Business logic
                    if (photo != null && photo.albumId == 1) {
                         RetriveImageTask(WeakReference(photoView))
                                 .execute(photo.url)
                    }

                } else {
                    showErrorAlert("Opps ! Something went wrong")
                }

            }

            override fun onFailure(call: Call<Photo>?, t: Throwable?) {
                //Handle fail request here
            }
        })

    }

    fun showErrorAlert(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }


    private class RetriveImageTask(photoView: WeakReference<ImageView>) : AsyncTask<String, Void, Bitmap>() {

        private var exception: Exception? = null

        private var view: WeakReference<ImageView> = photoView
        override fun doInBackground(vararg args: String): Bitmap? {
            val url = URL(args[0])
            return try {
                BitmapFactory.decodeStream(url.openConnection().getInputStream())
            } catch (e: Exception) {
                this.exception = e
                null
            }
        }

        override fun onPostExecute(bitmap: Bitmap) {
            if (view != null && bitmap != null) {
                view.get()?.setImageBitmap(bitmap)
            }
        }
    }
}
