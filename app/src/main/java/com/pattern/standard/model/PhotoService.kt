package com.pattern.standard.model

import retrofit2.Call
import retrofit2.http.GET

interface PhotoService{
    @GET("photos/1")
    fun getPhoto(): Call<Photo>
}