package com.pattern.standard.model

import retrofit2.Call

data class Photo(val albumId: Int,
                 val id: Int,
                 val title: String,
                 val url: String,
                 val thumbnailUrl: String){
    companion object {
        fun getPhotoCall(service: PhotoService): Call<Photo> {
            return service.getPhoto()
        }
    }

}

